<?php

namespace Drupal\Tests\vault_key_kv\Unit;

use Drupal\Core\Form\FormState;
use Drupal\key\KeyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\VaultClientInterface;
use Drupal\vault_key_kv\Plugin\KeyProvider\VaultKeyValueKeyProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\Exceptions\RequestException;
use Vault\ResponseModels\Response;

/**
 * Tests the Vault KV Value Key Provider plugin.
 *
 * @group vault_key_kv
 *
 * @covers \Drupal\vault_key_kv\Plugin\KeyProvider\VaultKeyValueKeyProvider
 * @codeCoverageIgnore
 */
class VaultKVValueProviderTest extends UnitTestCase {

  /**
   * VaultClint mock.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected VaultClientInterface|MockObject $vaultClientMock;

  /**
   * LoggerInterface mock.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $loggerMock;

  /**
   * The VaultKeyValueKeyProvider plugin.
   *
   * @var \Drupal\vault_key_kv\Plugin\KeyProvider\VaultKeyValueKeyProvider
   */
  protected VaultKeyValueKeyProvider $plugin;

  /**
   * Default plugin configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * List of mount points returned by listSecretEngineMounts mock.
   *
   * @var array
   */
  protected array $mountValues;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->configuration = [
      'secret_engine_mount' => 'secret/',
      'secret_path_prefix' => '',
    ];

    $this->vaultClientMock = $this->createMock(VaultClientInterface::class);

    $this->mountValues = [
      "kv1/" => [
        "accessor" => "kv_e6f94dd9",
        "config" => [
          "default_lease_ttl" => 0,
          "force_no_cache" => FALSE,
          "max_lease_ttl" => 0,
        ],
        "deprecation_status" => "supported",
        "description" => "",
        "external_entropy_access" => FALSE,
        "local" => FALSE,
        "options" => [
          "version" => "1",
        ],
        "plugin_version" => "",
        "running_plugin_version" => "v0.20.0+builtin",
        "running_sha256" => "",
        "seal_wrap" => FALSE,
        "type" => "kv",
        "uuid" => "20508da7-6d1c-c839-5295-5a0833b31ca8",
      ],
      "some_other_secret_mount/" => [
        "accessor" => "kv_c3054de4",
        "config" => [
          "default_lease_ttl" => 0,
          "force_no_cache" => FALSE,
          "max_lease_ttl" => 0,
        ],
        "deprecation_status" => "supported",
        "description" => "",
        "external_entropy_access" => FALSE,
        "local" => FALSE,
        "options" => [
          "version" => "2",
        ],
        "plugin_version" => "",
        "running_plugin_version" => "v0.20.0+builtin",
        "running_sha256" => "",
        "seal_wrap" => FALSE,
        "type" => "kv",
        "uuid" => "99da1e9f-7dec-208e-d049-2ea1de1d96cc",
      ],
      "secret/" => [
        "accessor" => "kv_97479f7a",
        "config" => [
          "default_lease_ttl" => 0,
          "force_no_cache" => FALSE,
          "max_lease_ttl" => 0,
        ],
        "deprecation_status" => "supported",
        "description" => "key/value secret storage",
        "external_entropy_access" => FALSE,
        "local" => FALSE,
        "options" => [
          "version" => "2",
        ],
        "plugin_version" => "",
        "running_plugin_version" => "v0.20.0+builtin",
        "running_sha256" => "",
        "seal_wrap" => FALSE,
        "type" => "kv",
        "uuid" => "eeaf40c7-8025-2c0e-5211-6f871bbf0bcd",
      ],
    ];

    $this->vaultClientMock
      ->method('listSecretEngineMounts')
      ->willReturn($this->mountValues);

    $this->loggerMock = $this->createMock(LoggerInterface::class);

    $settings = [
      'vault.settings' => [
        'base_url' => 'http://vault:8200',
      ],
    ];

    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject $config_factory_stub */
    $config_factory_stub = $this->getConfigFactoryStub($settings);
    $this->plugin = new VaultKeyValueKeyProvider($this->configuration, 'vault_kv', [], $this->vaultClientMock, $this->loggerMock, $this->getStringTranslationStub(), $config_factory_stub);

  }

  /**
   * Validate the plugin was constructed during setUp().
   */
  public function testValidatePluginInstance(): void {
    $this->assertInstanceOf(VaultKeyValueKeyProvider::class, $this->plugin, 'Plugin is a VaultKeyValueKeyProvider');
  }

  /**
   * Test the plugin create() method.
   */
  public function testCreate(): void {

    $settings = [
      'vault.settings' => [
        'base_url' => 'http://vault:8200',
      ],
    ];

    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject $config_factory_stub */
    $config_factory_stub = $this->getConfigFactoryStub($settings);

    $service_map = [
      [
        'vault.vault_client',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->vaultClientMock,
      ],
      [
        'logger.channel.vault',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->loggerMock,
      ],
      [
        'string_translation',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->getStringTranslationStub(),
      ],
      [
        'config.factory',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $config_factory_stub,
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = VaultKeyValueKeyProvider::create($container_mock, $this->configuration, 'vault_kv', []);
    $this->assertInstanceOf(VaultKeyValueKeyProvider::class, $plugin, 'Create returns VaultKeyValueKeyProvider plugin');
  }

  /**
   * Test obtain Default Configuration.
   */
  public function testDefaultConfiguration(): void {
    $this->assertIsArray($this->plugin->defaultConfiguration(), 'Default Configuration returns array');
  }

  /**
   * Test obtain key values.
   */
  public function testGetKeyValue(): void {

    $this->vaultClientMock->method('read')
      ->willReturnCallback([$this, 'getKeyValueResponse']);

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');

    $this->assertEquals('my_value', $this->plugin->getKeyValue($key_mock), 'Successful test returned correct data');
    $this->assertEmpty($this->plugin->getKeyValue($key_mock), 'Request error returned empty string');
    $this->assertEmpty($this->plugin->getKeyValue($key_mock), 'Invalid stored data returned empty string');
    $this->assertEmpty($this->plugin->getKeyValue($key_mock), 'No data stored returned empty string');
  }

  /**
   * Provides mock data for Vault calls in getKeyValue().
   *
   * @param string $path
   *   Ignored path.
   *
   * @return \Vault\ResponseModels\Response
   *   Mock Vault response.
   *
   * @throws \Vault\Exceptions\RequestException
   *   Exception on mock request error.
   */
  public function getKeyValueResponse(string $path): Response {

    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['data' => ['value' => 'my_value']]]);

      case 2;
        throw new RequestException();

      case 3:
        return new Response(['data' => ['data' => ['not_value' => 'not_valid']]]);

      case 4:
        return new Response(['data' => ['not_data' => 'invalid_string']]);

      default:
        throw new \Exception('unexpected call to getKeyValueResponse');
    }
  }

  /**
   * Test set key values.
   */
  public function testSetKeyValue(): void {

    $this->vaultClientMock->method('write')
      ->with('/secret/data/test_key', ['data' => ['value' => 'data']])
      ->willReturnCallback([$this, 'setKeyValueResponseProvider']);

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');

    $this->assertTrue($this->plugin->setKeyValue($key_mock, 'data'), 'Set key value returned TRUE on success');
    $this->assertFalse($this->plugin->setKeyValue($key_mock, 'data'), 'Set key value returned FALSE on request exception');
  }

  /**
   * Provides mock data for Vault calls in setKeyValue().
   *
   * @param string $path
   *   Ignored path.
   * @param array $data
   *   Ignored data.
   *
   * @return \Vault\ResponseModels\Response
   *   Mock Vault response.
   *
   * @throws \Vault\Exceptions\RequestException
   *   Exception on mock request error.
   */
  public function setKeyValueResponseProvider(string $path, array $data): Response {

    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['data' => ['value' => 'my_value']]]);

      case 2;
        throw new RequestException();

      default:
        throw new \Exception('Unexpected call to setKeyValueResponseProvider');
    }
  }

  /**
   * Test set key values.
   */
  public function testDeleteKeyValue(): void {

    $this->vaultClientMock->method('delete')
      ->with('/secret/metadata/test_key')
      ->willReturnCallback([$this, 'deleteKeyValueResponseProvider']);

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');

    $this->assertTrue($this->plugin->deleteKeyValue($key_mock), 'Delete key value returned TRUE on success');
    $this->assertFalse($this->plugin->deleteKeyValue($key_mock), 'Delete key value returned FALSE on request exception');
  }

  /**
   * Provides mock data for Vault calls in setKeyValue().
   *
   * @param string $path
   *   Ignored path.
   *
   * @return \Vault\ResponseModels\Response
   *   Mock Vault response.
   *
   * @throws \Vault\Exceptions\RequestException
   *   Exception on mock request error.
   */
  public function deleteKeyValueResponseProvider(string $path): Response {

    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['data' => ['value' => 'my_value']]]);

      case 2;
        throw new RequestException();

      default:
        throw new \Exception('Unexpected call to setKeyValueResponseProvider');
    }
  }

  /**
   * Test build configuration form.
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $this->assertIsArray($form, 'Form builder returns an array for form');
    $this->assertArrayNotHasKey('kv1/', $form['secret_engine_mount']['#options'], "v1 KV store should not be available");
  }

  /**
   * Test build configuration form.
   *
   * @dataProvider providerValidateConfigForm
   */
  public function testValidateConfigurationForm(string $path_prefix, int $expected_error_count): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $form_state->setValue('secret_path_prefix', $path_prefix);
    $this->plugin->validateConfigurationForm($form, $form_state);
    $this->assertCount($expected_error_count, $form_state->getErrors(), "Expected error count matched");
  }

  /**
   * Provide data for tetValidateConfigurationForm().
   */
  public static function providerValidateConfigForm(): array {
    return [
      'Single folder' => [
        'test',
        0,
      ],
      'Under multiple folders' => [
        'test/test1/test',
        0,
      ],
      'Contains invalid characters' => [
        'contains/invalid/ | /characters',
        1,
      ],
      'Empty prefix' => [
        '',
        0,
      ],
    ];
  }

  /**
   * Test submitConfigurationForm.
   */
  public function testSubmitConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $values = [
      'secret_path_prefix' => 'test1/test2',
    ];
    $form_state->setValues($values);
    $this->plugin->submitConfigurationForm($form, $form_state);
    $this->assertEquals($values, $this->plugin->getConfiguration(), 'Configuration returns as set');
  }

}
