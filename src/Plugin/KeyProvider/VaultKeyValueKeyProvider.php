<?php

namespace Drupal\vault_key_kv\Plugin\KeyProvider;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\vault\VaultClient;
use Drupal\vault\VaultClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;
use Psr\Log\LoggerInterface;

/**
 * Adds a key provider that allows a key to be stored in HashiCorp Vault.
 *
 * @KeyProvider(
 *   id = "vault_kv",
 *   label = "Vault Key/Value",
 *   description = @Translation("This provider stores the key in HashiCorp Vault key/value secret engine."),
 *   storage_method = "vault_kv",
 *   key_value = {
 *     "accepted" = TRUE,
 *     "required" = TRUE
 *   }
 * )
 */
final class VaultKeyValueKeyProvider extends KeyProviderBase implements KeyProviderSettableValueInterface, KeyPluginFormInterface {

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $settings;

  /**
   * The Vault client.
   *
   * @var \Drupal\vault\VaultClientInterface
   */
  protected VaultClientInterface $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Vault service config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $vaultConfig;

  /**
   * Constructs a VaultKeyValeKeyProvider object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface $vault_client
   *   A Drupal Vault module client.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String Translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config_factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VaultClientInterface $vault_client, LoggerInterface $logger, TranslationInterface $string_translation, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setClient($vault_client);
    $this->setLogger($logger);
    $this->setStringTranslation($string_translation);

    $this->vaultConfig = $config_factory->get('vault.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vault.vault_client'),
      $container->get('logger.channel.vault'),
      $container->get('string_translation'),
      $container->get('config.factory')
    );
  }

  /**
   * Sets client property.
   *
   * @param \Drupal\vault\VaultClientInterface $client
   *   The vault client.
   *
   * @return $this
   *   Current object.
   */
  public function setClient(VaultClientInterface $client): static {
    $this->client = $client;
    return $this;
  }

  /**
   * Sets logger property.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   *
   * @return self
   *   Current object.
   */
  public function setLogger(LoggerInterface $logger): VaultKeyValueKeyProvider {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'secret_engine_mount' => 'secret/',
      'secret_path_prefix' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    $path = $this->buildRequestPath("get", $key);
    try {
      $response = $this->client->read($path);
      $response_data = $response->getData();
      if (!is_array($response_data) || !array_key_exists('data', $response_data) || !is_array($response_data['data'])) {
        return '';
      }
      $data = $response_data['data'];
      return $data['value'] ?? '';
    }
    catch (\Exception $e) {
      $this->logger->critical('Unable to fetch secret ' . $key->id());
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyValue(KeyInterface $key, $key_value): bool {
    $path = $this->buildRequestPath("set", $key);
    try {
      $this->client->write($path, ['data' => ['value' => $key_value]]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->critical('Unable to write secret ' . $key->id());
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyValue(KeyInterface $key): bool {
    $path = $this->buildRequestPath("delete", $key);
    try {
      $this->client->delete($path);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->critical('Unable to delete secret ' . $key->id());
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $provider_config = $this->getConfiguration();
    $new = empty($form_state->getStorage()['key_value']['current']);

    $form['secret_engine_mount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Engine Mount'),
      '#description' => $this->t('The Key/Value secret engine mount point.'),
      '#field_prefix' => sprintf('%s/%s/', $this->vaultConfig->get('base_url'), VaultClient::API),
      '#required' => TRUE,
      '#default_value' => $provider_config['secret_engine_mount'],
      '#disabled' => !$new,
    ];

    $form['secret_path_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Path Prefix'),
      '#description' => $this->t('The path prefix where the secret should be stored. The key machine name will be appended to this value. There may be policy restrictions at certain paths.'),
      '#default_value' => $provider_config['secret_path_prefix'],
      '#disabled' => !$new,
    ];

    try {
      // Attempt to provide better UX by listing the available mounts. If this
      // fails it will fall back to the standard textfied input.
      $mount_points = $this->client->listSecretEngineMounts(['kv']);

      $form['secret_engine_mount']['#type'] = 'select';
      $form['secret_engine_mount']['#options'] = [];
      foreach ($mount_points as $mount => $info) {
        if (
          !is_array($info)
          || !array_key_exists('options', $info)
          || !is_array($info['options'])
          || !array_key_exists('version', $info['options'])
          || !is_string($info['options']['version'])
          || $info['options']['version'] != "2"
        ) {
          // This is not a KV2 store do not list.
          continue;
        }
        $form['secret_engine_mount']['#options'][$mount] = $mount;
      }
    }
    catch (\Exception $e) {
      $this->logger->error("Unable to list mount points for key/value secret engine");
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $key_provider_settings = $form_state->getValues();

    // Ensure secret path is only url safe characters.
    $prefix = $key_provider_settings['secret_path_prefix'];
    if (!UrlHelper::isValid($prefix, FALSE) && $prefix !== "") {
      $form_state->setErrorByName('secret_path_prefix', $this->t('Secret Path Prefix must use only valid uri characters'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Builds the URL endpoint.
   *
   * @param string $action
   *   Action being performed. One of "get", "set", "delete".
   * @param \Drupal\key\KeyInterface $key
   *   Key entity.
   *
   * @return string
   *   Endpoint URL for request action.
   */
  protected function buildRequestPath(string $action, KeyInterface $key): string {
    $provider_config = $this->getConfiguration();

    $placeholders = [
      ':secret_engine_mount' => $provider_config['secret_engine_mount'],
      ':endpoint' => '',
      ':secret_path' => implode('/', array_filter([
        trim($provider_config['secret_path_prefix'], '/'),
        $key->id(),
      ])),
    ];
    switch ($action) {
      case 'get':
      case 'set':
        $placeholders[':endpoint'] = 'data';
        break;

      case 'delete':
        $placeholders[':endpoint'] = 'metadata';
        break;
    }
    $url = new FormattableMarkup("/:secret_engine_mount:endpoint/:secret_path", $placeholders);
    return (string) $url;
  }

}
