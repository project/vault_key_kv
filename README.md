CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Previous Maintainers

INTRODUCTION
------------

This module provides support for the Key module to obtain secrets from the Vault
K/V Secrets Backend.

* For the full description of the module visit:
  https://www.drupal.org/project/vault_key_kv

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/vault_key_kv

REQUIREMENTS
------------

This module requires the following modules:

* [Vault](https://www.drupal.org/project/issues/vault)
* [Key](https://www.drupal.org/project/key)

INSTALLATION
------------

Install the Vault Key KV module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

Please refer to the Key Module documentation for details on configuring keys.

1. To use this module select a Key Provider of "Vault Key/Value"
2. Select the path from the secret engine dropdown that refers to your K/V
   secrets storage.
3. Provide the remaining path details on where the secret should be stored.
4. Provide a value to store in the key. This value will be stored in the Vault.

MAINTAINERS
-----------

* Conrad Lara - https://www.drupal.org/u/cmlara

PREVIOUS MAINTAINERS
--------------------
Nick Santamaria - https://www.drupal.org/u/nicksanta
